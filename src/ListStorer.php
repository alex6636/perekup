<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   18.04.2018
 */

namespace perekup;

class ListStorer
{
    protected $parsed_data;
    protected $data_dir;

    /**
     * @param Car[] $parsed_data
     * @param string $data_dir
     */
    public function __construct($parsed_data, $data_dir) {
        $this->parsed_data = $parsed_data;
        $this->data_dir = $data_dir;
        if (!is_dir($this->data_dir)) {
            mkdir($this->data_dir);
        }
    }

    public function storeParsedData() {
        $file_path = $this->getStoredDataFilePath();
        $serialized = serialize($this->parsed_data);
        return file_put_contents($file_path, $serialized);
    }
    
    public function getStoredParsedData() {
        $file_path = $this->getStoredDataFilePath();
        if (is_file($file_path)) {
            return unserialize(file_get_contents($file_path));
        }
        return [];
    }
    
    public function getStoredDataFilePath() {
        // new data an every day
        return $this->data_dir . '/' . date('d-m-Y') . '.txt';
    }
}
