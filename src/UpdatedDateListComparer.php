<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   29.04.2018
 */

namespace perekup;

class UpdatedDateListComparer extends ListComparer
{
    /**
     * @return Car[]
     */
    public function compare() {
        // filter cars without an updated date
        /** @var Car[] $parsed_date_filtered */
        $parsed_date_filtered = [];
        foreach ($this->parsed_data as $k=>$Car) {
            if ($Car->updated_date !== null) {
                $parsed_date_filtered[] = $Car;
            }
        }
        if (empty($this->stored_data)) {
            return $parsed_date_filtered;
        }
        $updated_cars = [];
        foreach ($parsed_date_filtered as $ParsedCar) {
            foreach ($this->stored_data as $StoredCar) {
                if ($ParsedCar->id == $StoredCar->id) {
                    if ($ParsedCar->updated_date != $StoredCar->updated_date) {
                        $updated_cars[] = $ParsedCar;
                    }
                    break;
                }
            }
        }
        return $updated_cars;
    }
}
