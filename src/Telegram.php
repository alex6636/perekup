<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   16.04.2018
 */

namespace perekup;
use TelegramBot\Api\BotApi;

class Telegram
{
    public $debug;
    
    protected $bot_api_token;
    protected $BotApi = null;
    
    public function __construct($bot_api_token, $debug = false) {
        $this->bot_api_token = $bot_api_token;
        $this->debug = $debug;
    }

    /**
     * @param string $chat_id
     * @param string $message
     * @return \TelegramBot\Api\Types\Message|null
     * @throws \TelegramBot\Api\InvalidArgumentException
     * @throws \TelegramBot\Api\Exception
     */
    public function sendMessage($chat_id, $message) {
        if ($this->debug) {
            echo $message;
            return null;
        }
        $BotApi = $this->getBotApi();
        return $BotApi->sendMessage($chat_id, $message);
    }
    
    /**
     * @param string $chat_id
     * @param Car[] $cars
     * @return \TelegramBot\Api\Types\Message
     * @throws \TelegramBot\Api\InvalidArgumentException
     * @throws \TelegramBot\Api\Exception
     */
    public function sendCarsMessage($chat_id, $cars) {
        $message_parts = [];
        foreach ($cars as $Car) {
            $message_parts[] = $this->formatCarMessage($Car);
        }
        $message = implode("\n\n", $message_parts);
        return $this->sendMessage($chat_id, $message);
    }
    
    /**
     * @param string $chat_id
     * @param Car[] $cars
     * @return \TelegramBot\Api\Types\Message
     * @throws \TelegramBot\Api\InvalidArgumentException
     * @throws \TelegramBot\Api\Exception
     */
    public function sendUpdatedCarsMessage($chat_id, $cars) {
        $message_parts = [];
        foreach ($cars as $Car) {
            $message_parts[] = 'ОБНОВЛЕНО: ' . $this->formatCarMessage($Car);
        }
        $message = implode("\n\n", $message_parts);
        return $this->sendMessage($chat_id, $message);
    }
    
    /**
     * @return BotApi
     */
    public function getBotApi() {
        if ($this->BotApi === null) {
            $this->BotApi = new BotApi($this->bot_api_token);
        }
        return $this->BotApi;
    }

    /**
     * @param Car $Car
     * @return string
     */
    protected function formatCarMessage(Car $Car) {
        $simple_parts = implode(' ', array_filter([$Car->title, $Car->year, $Car->price, $Car->location]));
        return $simple_parts . ($Car->date ? ' (' . $Car->date . ')' : '') . " \n" . $Car->url;
    }
}
