<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   22.08.2018
 */

namespace perekup;
use Sunra\PhpSimple\HtmlDomParser;
use simplehtmldom_1_5\simple_html_dom;
use simplehtmldom_1_5\simple_html_dom_node;

class KonfiskatParser extends Parser
{
    /** @var int|null $limit */
    public $limit = null;

    /**
     * Recursive list parsing
     * 
     * @param string $url
     * @return Car[]
     * @throws \Exception
     */
    public function parseList($url) {
        $contents = $this->getContents($url);
        if (!$contents) {
            throw new \Exception('Unable to connect to ' . $url);
        }
        static $parsed_list = [];
        static $item_number = 0;
        $dom = HtmlDomParser::str_get_html($contents);
        $wrapper = $dom->find('#catalog', 0);
        $items = $wrapper->find('.bx_catalog_item');
        if (!empty($items)) {
            foreach($items as $item) {
                if ($this->limit !== null) {
                    if ($item_number === $this->limit) {
                        break;
                    }
                    ++ $item_number;
                }
                $parsed_list[] = $this->parseListItem($item, $url);
            }
            if ($next_page_url = $this->parsePager($dom, $url)) {
                return $this->parseList($next_page_url);
            }   
        }
        return $parsed_list;
    }

    /**
     * @param simple_html_dom_node $item
     * @param string $url
     * @return Car
     */
    protected function parseListItem(simple_html_dom_node $item, $url) {
        $Car = new Car;
        $container = $item->find('.bx_catalog_item_container', 0);
        $id = $container->id;
        $title = trim($item->find('.p-name a', 0)->plaintext);
        $url = $url . '#' . $id;
        $price_parts = explode('(', $item->find('.bx_price', 0)->plaintext);
        $price = trim($price_parts[0]);
        $location = '';
        if ($descr_ul = $item->find('ul', 0)) {
            if ($desc_li_location = $descr_ul->find('li', 3)) {
                $location_parts = explode(':', $desc_li_location->plaintext);
                if (isset($location_parts[1])) {
                    $_location = trim($location_parts[1]);
                    $location = preg_replace('/\s+,\s+/i', ', ', $_location);
                }
            }
        }
        $Car->id = $id;
        $Car->url = $url;
        $Car->title = $title;
        $Car->price = $price;
        $Car->location = $location;
        return $Car;
    }

    /**
     * Returned URL to the next page or null
     * 
     * @param simple_html_dom $dom
     * @param string $url
     * @return string|null
     */
    protected function parsePager(simple_html_dom $dom, $url) {
        /** @var simple_html_dom_node|null $wrapper */
        /** @var simple_html_dom_node|null $active */
        /** @var simple_html_dom_node|null $next */
        /** @var simple_html_dom_node|null $a */
        if (!$wrapper = $dom->find('font.text', 1)) {
            return null;
        }
        $links = $wrapper->find('a');
        if (!empty($links)) {
            foreach ($links as $a) {
                if ($a->plaintext == 'След.') {
                    return 'http://' . parse_url($url)['host'] . $a->href;
                }
            }
        }
        return null;
    }
}
