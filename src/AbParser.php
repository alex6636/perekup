<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11.05.2018
 */

namespace perekup;

class AbParser extends Parser
{
    /** @var int|null $limit */
    public $limit = null;

    /**
     * Recursive list parsing
     *
     * @param string $url
     * @param string $params
     * @return Car[]
     * @throws \Exception
     */
    public function parseListToday($url, $params) {
        $result = [];
        $parsed_list = $this->parseList($url, $params);
        $today = date('Y-m-d');
        foreach ($parsed_list as $Car) {
            if ($Car->date !== $today) {
                break;
            }
            $result[] = $Car;
        }
        return $result;
    }

    /**
     * Recursive list parsing
     * 
     * @param string $url
     * @param string $params
     * @return Car[]
     * @throws \Exception
     */
    public function parseList($url, $params) {
        $contents = $this->getContentsXMLHttpRequestPost($url, $params);
        if (!$contents || !isset($contents->result->advertisements)) {
            throw new \Exception('Unable to connect to ' . $url . ' ' . $params);
        }
        static $parsed_list = [];
        static $item_number = 0;
        $items = $contents->result->advertisements;
        $total = (int) $contents->result->counters->total;
        if (!empty($items)) {
            foreach($items as $item) {
                if ($this->limit !== null) {
                    if ($item_number === $this->limit) {
                        break;
                    }
                }
                ++ $item_number;
                $parsed_list[] = $this->parseListItem($item);
            }
            if ($item_number < $total) {
                parse_str($params, $params_arr);
                $params_arr['page'] = isset($params_arr['page']) ? $params_arr['page'] + 1 : 2;
                return $this->parseList($url, http_build_query($params_arr));
            }
        }
        return $parsed_list;
    }

    /**
     * @param \stdClass $item
     * @return Car
     */
    protected function parseListItem(\stdClass $item) {
        $Car = new Car;
        $Car->id = $item->id;
        $Car->url = 'https://ab.onliner.by/car/' . $item->id;
        $Car->title = $item->title;
        $Car->year = $item->car->year;
        $Car->date = date('Y-m-d', strtotime($item->creationDate->date));
        return $Car;
    }
}
