<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   18.04.2018
 */

namespace perekup;

class UpdatedDateListStorer extends ListStorer
{
    public function storeParsedData() {
        // filter cars without an updated date
        $parsed_date_filtered = [];
        foreach ($this->parsed_data as $k=>$Car) {
            if ($Car->updated_date !== null) {
                $parsed_date_filtered[] = $Car;
            }
        }
        $file_path = $this->getStoredDataFilePath();
        $serialized = serialize($parsed_date_filtered);
        return file_put_contents($file_path, $serialized);
    }

    /**
     * @return string
     */
    public function getStoredDataFilePath() {
        return $this->data_dir . '/data.txt';
    }
}
