<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   01.05.2018
 */

namespace perekup;
use Katzgrau\KLogger\Logger;
use Psr\Log\LogLevel;

abstract class Parser
{
    public $logs;
    
    protected $logs_options = [];
    
    /** @var Logger|null $Logger */
    protected $Logger = null;
    
    /**
     * @param bool $logs
     * @param array $logs_options
     */
    public function __construct($logs = true, array $logs_options = []) {
        $this->logs = $logs;   
        $this->logs_options = $logs_options;
    }

    /**
     * @param string $url
     * @param bool $try_again
     * @return string|false
     */
    public function getContents($url, $try_again = true) {
        $curl_options = [
            CURLOPT_TIMEOUT=>99,
            CURLOPT_USERAGENT=>'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)',
            CURLOPT_HEADER=>1,
            CURLOPT_RETURNTRANSFER=>1,
        ];
        $ch = curl_init($url);
        curl_setopt_array($ch, $curl_options);
        $response = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        curl_close($ch);
        if (!$curl_errno) {
            $this->log('OK: ' . $url);
            return $response;
        } else {
            // an error occured
            $this->log('ERR "' . curl_error($ch) . '": ' . $url);
            if ($try_again) {
                $this->log('TRY AGAIN: ' . $url);
                return $this->getContents($url, false);
            }
        }
        return false;
    }

    /**
     * @param string $url
     * @param string $params
     * @param bool $try_again
     * @return \stdClass|false
     */
    public function getContentsXMLHttpRequestPost($url, $params, $try_again = true) {
        $parsed_url = parse_url($url);
        $curl_options = [
            CURLOPT_TIMEOUT=>99,
            CURLOPT_HTTPHEADER=>[
                'Host' => $parsed_url['host'],
                'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1',
                'Accept' => 'application/json, text/javascript, */*; q=0.01',
                'Accept-Language' => 'en-us,en;q=0.5',
                'Accept-Encoding' => 'gzip, deflate',
                'Accept-Charset' => 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
                'Keep-Alive' => '115',
                'Connection' => 'keep-alive',
                'X-Requested-With' => 'XMLHttpRequest',
                'Referer' => $parsed_url['scheme'] . '://' . $parsed_url['host']
            ],
            CURLOPT_HEADER=>0,
            CURLOPT_URL =>$url,
            CURLOPT_POST=>1,
            CURLOPT_POSTFIELDS=>$params,
            CURLOPT_RETURNTRANSFER=>1,
        ];
        $ch = curl_init();
        curl_setopt_array($ch, $curl_options);
        $response = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        curl_close($ch);
        if (!$curl_errno) {
            $this->log('OK: ' . $url . ' ' . $params);
            return json_decode($response);
        } else {
            // an error occured
            $this->log('ERR "' . curl_error($ch) . '": ' . $url . ' ' . $params);
            if ($try_again) {
                $this->log('TRY AGAIN: ' . $url . ' ' . $params);
                return $this->getContentsXMLHttpRequestPost($url, $params, false);
            }
        }
        return false;
    }

    /**
     * @return Logger
     */
    public function getLogger() {
        if ($this->Logger === null) {
            $this->Logger = new Logger(
                $_SERVER['DOCUMENT_ROOT'] . '/../logs', 
                LogLevel::DEBUG,
                $this->logs_options
            );
        }
        return $this->Logger;
    }

    /**
     * @param string $message
     */
    public function log($message) {
        if ($this->logs) {
            $this->getLogger()->debug($message);
        }        
    }
}
