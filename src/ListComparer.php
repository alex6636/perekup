<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   16.04.2018
 */

namespace perekup;

class ListComparer
{
    protected $parsed_data;
    protected $stored_data;

    /**
     * @param Car[] $parsed_data
     * @param Car[] $stored_data
     */
    public function __construct($parsed_data, $stored_data) {
        $this->parsed_data = $parsed_data;
        $this->stored_data = $stored_data;
    }

    /**
     * @return Car[]
     */
    public function compare() {
        $stored_data_ids = [];
        foreach ($this->stored_data as $Car) {
            $stored_data_ids[] = $Car->id;
        }
        return array_filter($this->parsed_data, function($_Car) use ($stored_data_ids){
            return !in_array($_Car->id, $stored_data_ids);
        });
    }
}
