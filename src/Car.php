<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   16.04.2018
 */

namespace perekup;

class Car
{
    public $id;
    public $url;
    public $title;
    public $year;
    public $price;
    public $location;
    public $date;
    public $updated_date;
}
