<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   15.04.2018
 */

namespace perekup;
use Sunra\PhpSimple\HtmlDomParser;
use simplehtmldom_1_5\simple_html_dom;
use simplehtmldom_1_5\simple_html_dom_node;

class AvParser extends Parser
{
    /** @var int|null $limit */
    public $limit = null;

    /**
     * Recursive list parsing
     * 
     * @param string $url
     * @return Car[]
     * @throws \Exception
     */
    public function parseList($url) {
        $contents = $this->getContents($url);
        if (!$contents) {
            throw new \Exception('Unable to connect to ' . $url);
        }
        static $parsed_list = [];
        static $item_number = 0;
        $dom = HtmlDomParser::str_get_html($contents);
        $wrapper = $dom->find('.listing-wrap', 0);
        $items = $wrapper->find('.listing-item');
        if (!empty($items)) {
            foreach($items as $item) {
                if ($this->limit !== null) {
                    if ($item_number === $this->limit) {
                        break;
                    }
                    ++ $item_number;
                }
                $parsed_list[] = $this->parseListItem($item);
            }
            if ($next_page_url = $this->parsePager($dom)) {
                return $this->parseList($next_page_url);
            }   
        }
        return $parsed_list;
    }

    /**
     * Recursive list parsing an parsing an each page of the car
     * 
     * @param string $url
     * @return Car[]
     * @throws \Exception
     */
    public function parseListAdvanced($url) {
        $parsed_list = $this->parseList($url);
        if (!empty($parsed_list)) {
            foreach ($parsed_list as $Car) {
                $this->parsePageItem($Car);
            }
        }
        return $parsed_list;
    }
    
    /**
     * @param simple_html_dom_node $item
     * @return Car
     */
    protected function parseListItem(simple_html_dom_node $item) {
        $Car = new Car;
        $link = $item->find('.listing-item-title a', 0);
        $params = $item->find('.listing-item-price', 0);
        $url = $link->href;
        $url_parts = explode('/', $link->href);
        $Car->id = $url_parts[count($url_parts) - 1];
        $Car->url = $url;
        $Car->title= trim($link->plaintext);
        $Car->year = trim($params->find('span', 0)->plaintext);
        $price_byn = $params->find('strong', 0)->plaintext;
        $price_usd = $params->find('small', 0)->plaintext;
        $Car->price = $price_usd ? $price_usd . '$' : $price_byn;
        $Car->location = $params->find('.listing-item-location', 0)->plaintext;
        // for NOT top cars
        if ($date = $item->find('.listing-item-date', 0)) {
            $Car->date = trim($date->plaintext);   
        }
        return $Car;
    }

    /**
     * Returned URL to the next page or null
     * 
     * @param simple_html_dom $dom
     * @return string|null
     */
    protected function parsePager(simple_html_dom $dom) {
        /** @var simple_html_dom_node|null $wrapper */
        /** @var simple_html_dom_node|null $active */
        /** @var simple_html_dom_node|null $next */
        /** @var simple_html_dom_node|null $a */
        if (!$wrapper = $dom->find('.pages-numbers', 0)) {
            return null;
        }
        if (!$active = $wrapper->find('.pages-numbers-item--active', 0)) {
            return null;
        }
        if (!$next = $active->next_sibling()) {
            return null;
        }
        if (!$a = $next->find('a', 0)) {
            return null;
        }
        return $a->href;
    }

    /**
     * @param Car $Car
     * @throws \Exception
     */
    protected function parsePageItem(Car $Car) {
        $contents = $this->getContents($Car->url);
        if (!$contents) {
            throw new \Exception('Unable to connect to ' . $Car->url);
        }
        $dom = HtmlDomParser::str_get_html($contents);
        // updated date
        if ($dates = $dom->find('.card-about-item-dates', 0)) {
            if ($dl = $dates->find('dl', 1)) {
                if ($dd = $dl->find('dd', 0)) {
                    $Car->updated_date = $dd->plaintext;
                }
            }
        }
    }
}
