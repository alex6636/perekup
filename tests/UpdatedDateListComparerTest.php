<?php
namespace perekup\tests;
use perekup\UpdatedDateListComparer;
use perekup\Car;
use PHPUnit\Framework\TestCase;

class UpdatedDateListComparerTest extends TestCase
{
    public function testCompare() {
        $Car1 = new Car();
        $Car1->id = 11;
        $Car1->url = 'av.by/11';
        $Car1->title = 'Audi A3';
        $Car1->price = '9000$';
        $Car1->updated_date = '12.05.2018';
        //
        $Car2 = new Car();
        $Car2->id = 12;
        $Car2->url = 'av.by/12';
        $Car2->title = 'BMW 3';
        $Car2->price = '8500$';
        $Car2->updated_date = '12.05.2018';
        //
        $Car3 = new Car();
        $Car3->id = 13;
        $Car3->url = 'av.by/13';
        $Car3->title = 'Volkswagen Passat';
        $Car3->price = '7000$';
        $Car3->updated_date = '12.05.2018';
        //
        $Car4 = new Car();
        $Car4->id = 14;
        $Car4->url = 'av.by/14';
        $Car4->title = 'Nissan Juke';
        $Car4->price = '6500$';
        $Car4->updated_date = '12.05.2018';
        //
        $Car5 = new Car();
        $Car5->id = 15;
        $Car5->url = 'av.by/14';
        $Car5->title = 'Mazda 6';
        $Car5->price = '7400$';
        $Car5->updated_date = '13.05.2018';
        //
        $stored_data = [$Car1, $Car2, $Car3, $Car4];
        $UpdatedCar1 = clone $Car1;
        $UpdatedCar4 = clone $Car4;
        $UpdatedCar1->updated_date = '14.05.2018';
        $UpdatedCar4->updated_date = '15.05.2018';
        $parsed_data = [$Car5, $UpdatedCar1, $Car2, $Car3, $UpdatedCar4];
        $UpdatedDateListComparer = new UpdatedDateListComparer($parsed_data, $stored_data);
        $this->assertEquals($UpdatedDateListComparer->compare(), [$UpdatedCar1, $UpdatedCar4]);
    }
}
