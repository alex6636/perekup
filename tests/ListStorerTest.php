<?php
namespace perekup\tests;
use perekup\ListStorer;
use perekup\Car;
use PHPUnit\Framework\TestCase;

class ListStorerTest extends TestCase
{
    public function testStoreAndGetStoredParsedData() {
        $Car1 = new Car();
        $Car1->id = 11;
        $Car1->url = 'av.by/11';
        $Car1->title = 'Audi A3';
        $Car1->price = '9000$';
        //
        $Car2 = new Car();
        $Car2->id = 12;
        $Car2->url = 'av.by/12';
        $Car2->title = 'BMW 3';
        $Car2->price = '8500$';
        
        $parsed_data = [$Car1, $Car2];
        $test_data_dir = __DIR__ . '/test_parsed_data';
        $ListStorer = new ListStorer($parsed_data, $test_data_dir);
        
        $this->assertTrue($ListStorer->storeParsedData() > 0);
        $this->assertEquals($parsed_data, $ListStorer->getStoredParsedData());
    }
}
