<?php
namespace perekup\tests;
use perekup\ListComparer;
use perekup\Car;
use PHPUnit\Framework\TestCase;

class ListComparerTest extends TestCase
{
    public function testSimpleCompare1() {
        list($Car1, $Car2, $Car3, $Car4) = $this->getCarsData();
        $parsed_data = [$Car4, $Car1, $Car2, $Car3];
        $stored_data = [$Car1,$Car2, $Car3];
        $ListComparer = new ListComparer($parsed_data, $stored_data);
        $this->assertEquals($ListComparer->compare(), [$Car4]);
    }
    
    public function testSimpleCompare2() {
        $parsed_data = $this->getCarsData();
        $stored_data = $parsed_data;
        $ListComparer = new ListComparer($parsed_data, $stored_data);
        $this->assertEquals($ListComparer->compare(), []);
    }
    
    public function testSimpleCompare3() {
        list($Car1, $Car2, $Car3, $Car4) = $this->getCarsData();
        $parsed_data = [$Car4, $Car1, $Car2, $Car3];
        $stored_data = [$Car1];
        $ListComparer = new ListComparer($parsed_data, $stored_data);
        $this->assertEquals($ListComparer->compare(), [$Car2, $Car3, $Car4], "\$canonicalize = true", $delta = 0.0, $maxDepth = 10, $canonicalize = true);
    }
    
    public function testShuffledCompare() {
        list($Car1, $Car2, $Car3, $Car4) = $this->getCarsData();
        $parsed_data = [$Car1, $Car4, $Car3, $Car2];
        $stored_data = [$Car2, $Car1];
        $ListComparer = new ListComparer($parsed_data, $stored_data);
        $this->assertEquals($ListComparer->compare(), [$Car3, $Car4], "\$canonicalize = true", $delta = 0.0, $maxDepth = 10, $canonicalize = true);
    }
    
    public function getCarsData() {
        $Car1 = new Car();
        $Car1->id = 11;
        $Car1->url = 'av.by/11';
        $Car1->title = 'Audi A3';
        $Car1->price = '9000$';
        //
        $Car2 = new Car();
        $Car2->id = 12;
        $Car2->url = 'av.by/12';
        $Car2->title = 'BMW 3';
        $Car2->price = '8500$';
        //
        $Car3 = new Car();
        $Car3->id = 13;
        $Car3->url = 'av.by/13';
        $Car3->title = 'Volkswagen Passat';
        $Car3->price = '7000$';
        //
        $Car4 = new Car();
        $Car4->id = 14;
        $Car4->url = 'av.by/14';
        $Car4->title = 'Nissan Juke';
        $Car4->price = '6500$';
        
        return [$Car1, $Car2, $Car3, $Car4];
    }
}
