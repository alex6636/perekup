<?php
namespace perekup\tests;
use perekup\UpdatedDateListStorer;
use perekup\Car;
use PHPUnit\Framework\TestCase;

class UpdatedDateListStorerTest extends TestCase
{
    public function testStoreAndGetStoredParsedData() {
        $Car1 = new Car();
        $Car1->id = 11;
        $Car1->url = 'av.by/11';
        $Car1->title = 'Audi A3';
        $Car1->price = '9000$';
        $Car1->updated_date = '12.05.2018';
        //
        $Car2 = new Car();
        $Car2->id = 12;
        $Car2->url = 'av.by/12';
        $Car2->title = 'BMW 3';
        $Car2->price = '8500$';
        //
        $Car3 = new Car();
        $Car3->id = 13;
        $Car3->url = 'av.by/13';
        $Car3->title = 'Volkswagen Passat';
        $Car3->price = '7000$';
        $Car3->updated_date = '15.05.2018';
        //
        $Car4 = new Car();
        $Car4->id = 14;
        $Car4->url = 'av.by/14';
        $Car4->title = 'Nissan Juke';
        $Car4->price = '6500$';
        
        $parsed_data = [$Car1, $Car2, $Car3, $Car4];
        $real_stored_data = [$Car1, $Car3];
        $test_data_dir = __DIR__ . '/test_parsed_data2';
        $ListStorer = new UpdatedDateListStorer($parsed_data, $test_data_dir);
        
        $this->assertTrue($ListStorer->storeParsedData() > 0);
        $this->assertEquals($real_stored_data, $ListStorer->getStoredParsedData());
    }
}
