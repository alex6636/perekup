<?php
// Load composer
require '../vendor/autoload.php';

date_default_timezone_set('Europe/Moscow');
define('PARSER_URL', 'https://ab.onliner.by/search');
define('PARSER_URL_PARAMS', 'min-price=9000&max-price=14000&min-year=2012&transmission[]=1&currency=USD&sort[]=creation_date&page=1&car[0][2]=&car[1][62]=&car[2][57]=');
define('PARSER_DATA_DIR', __DIR__ . '/../parser_data/mycars_onliner');
define('PARSER_LOGS', true);
define('PARSER_LOGS_OPTIONS', ['prefix'=>'mycars_onliner_']);
define('TELEGRAM_DEBUG', false);
define('TELEGRAM_BOT_API_TOKEN', '452201096:AAGWsbKzJNTH3iOob5iGqhF4W9IOp9i2UAc'); // @PerekupAlexsBot
define('TELEGRAM_CHAT_ID', '-1001108846404'); // PerekupChannel

use perekup\AbParser;
use perekup\Telegram;
use perekup\ListComparer;
use perekup\ListStorer;

$Parser = new AbParser(PARSER_LOGS, PARSER_LOGS_OPTIONS);
$Telegram = new Telegram(TELEGRAM_BOT_API_TOKEN, TELEGRAM_DEBUG);

try {
    $parsed_data = $Parser->parseListToday(PARSER_URL, PARSER_URL_PARAMS);
    $ListStorer = new ListStorer($parsed_data, PARSER_DATA_DIR);
    $stored_data = $ListStorer->getStoredParsedData();
    $ListComparer = new ListComparer($parsed_data, $stored_data);
    $new_cars = $ListComparer->compare();
    if (!empty($new_cars)) {
        $ListStorer->storeParsedData();
        try {
            $Telegram->sendCarsMessage(TELEGRAM_CHAT_ID, $new_cars);
        } catch (\TelegramBot\Api\InvalidArgumentException $Exception) {

        } catch (\TelegramBot\Api\Exception $Exception) {

        }
    }
    echo count($new_cars);
} catch (\Exception $Exception) {
    $Telegram->sendMessage(TELEGRAM_CHAT_ID, $Exception->getMessage());
    echo $Exception->getMessage();
}