<?php
// Load composer
require '../vendor/autoload.php';

date_default_timezone_set('Europe/Moscow');
define('PARSER_URL', 'https://cars.av.by/search?brand_id%5B0%5D=6&brand_id%5B1%5D=1126&brand_id%5B2%5D=1216&model_id%5B0%5D=0&model_id%5B1%5D=0&model_id%5B2%5D=0&year_from=2012&year_to=&currency=USD&price_from=9000&price_to=14000&transmission=1&body_id=&engine_volume_min=&engine_volume_max=&driving_id=&mileage_min=&mileage_max=&region_id=&interior_material=&interior_color=&exchange=&search_time=');
define('PARSER_DATA_DIR', __DIR__ . '/../parser_data/mycars_updated');
define('PARSER_LOGS', true);
define('PARSER_LOGS_OPTIONS', ['prefix'=>'mycars_updated_']);
define('TELEGRAM_DEBUG', false);
define('TELEGRAM_BOT_API_TOKEN', '452201096:AAGWsbKzJNTH3iOob5iGqhF4W9IOp9i2UAc'); // @PerekupAlexsBot
define('TELEGRAM_CHAT_ID', '-1001108846404'); // PerekupChannel

use perekup\AvParser;
use perekup\Telegram;
use perekup\UpdatedDateListComparer;
use perekup\UpdatedDateListStorer;

$Parser = new AvParser(PARSER_LOGS, PARSER_LOGS_OPTIONS);
$Telegram = new Telegram(TELEGRAM_BOT_API_TOKEN, TELEGRAM_DEBUG);

try {
    $parsed_data = $Parser->parseListAdvanced(PARSER_URL);
    $UpdatedDateListStorer = new UpdatedDateListStorer($parsed_data, PARSER_DATA_DIR);
    $stored_data = $UpdatedDateListStorer->getStoredParsedData();
    $UpdatedDateListComparer = new UpdatedDateListComparer($parsed_data, $stored_data);
    $updated_cars = $UpdatedDateListComparer->compare();
    if (!empty($updated_cars)) {
        $UpdatedDateListStorer->storeParsedData();
        try {
            $Telegram->sendUpdatedCarsMessage(TELEGRAM_CHAT_ID, $updated_cars);
        } catch (\TelegramBot\Api\InvalidArgumentException $Exception) {

        } catch (\TelegramBot\Api\Exception $Exception) {

        }
    }
    echo count($updated_cars);
} catch (\Exception $Exception) {
    $Telegram->sendMessage(TELEGRAM_CHAT_ID, $Exception->getMessage());
    echo $Exception->getMessage();
}
