<?php
// Load composer
require '../vendor/autoload.php';

date_default_timezone_set('Europe/Moscow');
define('PARSER_URL', 'http://konfiskat.by/konfiskat/13532/');
define('PARSER_DATA_DIR', __DIR__ . '/../parser_data/konfiskat_fishing');
define('PARSER_LOGS', true);
define('PARSER_LOGS_OPTIONS', ['prefix'=>'konfiskat_fishing_']);
define('TELEGRAM_DEBUG', false);
define('TELEGRAM_BOT_API_TOKEN', '452201096:AAGWsbKzJNTH3iOob5iGqhF4W9IOp9i2UAc'); // @PerekupAlexsBot
define('TELEGRAM_CHAT_ID', '-1001230844640'); // Konfiskat

use perekup\KonfiskatParser;
use perekup\Telegram;
use perekup\ListComparer;
use perekup\ListStorer;

$Parser = new KonfiskatParser(PARSER_LOGS, PARSER_LOGS_OPTIONS);
$Telegram = new Telegram(TELEGRAM_BOT_API_TOKEN, TELEGRAM_DEBUG);

try {
    $parsed_data = $Parser->parseList(PARSER_URL);
    $ListStorer = new ListStorer($parsed_data, PARSER_DATA_DIR);
    $stored_data = $ListStorer->getStoredParsedData();
    $ListComparer = new ListComparer($parsed_data, $stored_data);
    $new_cars = $ListComparer->compare();
    if (!empty($new_cars)) {
        $ListStorer->storeParsedData();
        try {
            $Telegram->sendCarsMessage(TELEGRAM_CHAT_ID, $new_cars);
        } catch (\TelegramBot\Api\InvalidArgumentException $Exception) {

        } catch (\TelegramBot\Api\Exception $Exception) {

        }
    }
    echo count($new_cars);
} catch (\Exception $Exception) {
    $Telegram->sendMessage(TELEGRAM_CHAT_ID, $Exception->getMessage());
    echo $Exception->getMessage();
}
